// console.log("Hello World!")

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let title = json.map(item => item.title)
 	console.log(title)
});
// GET method

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "GET",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: "false",
		title: "Delectus aut autes",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// POST method
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: "false",
		title: "Created to do list Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// PUT method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to to list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: "false",
		dateCompleted: "07/09/21",
		status: "Complete",
		title: "delectus aut autes",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


